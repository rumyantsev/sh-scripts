#!/bin/bash

# https://blog.amartynov.ru/linux-server-backup-to-webdav/

SERVER_NAME="tonussporta"
TIME=`date +%Y-%m-%d_%H:%M:%S`

# FS_FILE=~/backup/$SERVER_NAME-fs.$TIME.tar.gz
POSTGRES_FILE=~/backup/$SERVER_NAME-postgres.$TIME.sql.gz

# Archiving filesystem
# tar -czf $FS_FILE /etc /root /home

# Archiving databases
export PGPASSWORD=PASSWORD
pg_dump -U USER DATABASE | gzip > $POSTGRES_FILE

# Uploading to the cloud
curl --user USER:PASSWORD -T "{$POSTGRES_FILE}" https://webdav.yandex.ru/

# Cleanup
# unlink $FS_FILE
unlink $POSTGRES_FILE