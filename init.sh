#!/bin/bash

PROJECT_NAME='your@project_name'
ALIAS_NAME='your@alias_name'
DEPLOY_FILE='deploy.sh'
USER_NAME='your@user_name'

GIT_EMAIL='your@email.com'
GIT_NAME='your@name'

UWSGI_FILE='uwsgi.ini'

SETTINGS_SERVER='settings_server.sh'
SUPERVISOR='supervisor.conf'

NGINX_FILE='nginx'

# Enter the default folder
cd ~

# Create virtualenv
virtualenv $PROJECT_NAME
cd $PROJECT_NAME
source bin/activate

# Git settings
git init
git config receive.denyCurrentBranch ignore
git config --global user.email "$GIT_EMAIL"
git config --global user.name "$GIT_NAME"

# Create file Git hooks post-receive
echo "#!/bin/bash" >> .git/hooks/post-receive
echo "cd ~/$PROJECT_NAME" >> .git/hooks/post-receive
echo "GIT_DIR='.git'" >> .git/hooks/post-receive
echo "git reset --hard" >> .git/hooks/post-receive
echo "bash deploy.sh" >> .git/hooks/post-receive

chmod +x .git/hooks/post-receive

# Edit .bash_aliases
echo "alias $ALIAS_NAME='cd ~/$PROJECT_NAME/ && source bin/activate'" >> ~/.bash_aliases

# Create deploy file
echo "#!/bin/bash" >> $DEPLOY_FILE
echo "# Copy staticfiles and reload uwsgi" >> $DEPLOY_FILE
echo "cd ~/$PROJECT_NAME && source bin/activate && python www/manage.py collectstatic --noinput" >> $DEPLOY_FILE
echo "pip install -r req.txt" >> $DEPLOY_FILE
echo "touch reload_uwsgi" >> $DEPLOY_FILE
echo "sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl update" >> $DEPLOY_FILE
echo "# Reload nginx server" >> $DEPLOY_FILE
echo "sudo service nginx reload" >> $DEPLOY_FILE

# Mkdir
mkdir logs

# Create uwsgi file
echo "[uwsgi]" >> $UWSGI_FILE
echo "chdir = $PWD/www" >> $UWSGI_FILE
echo "module = django.core.wsgi:get_wsgi_application()" >> $UWSGI_FILE
echo "env = DJANGO_SETTINGS_MODULE=$PROJECT_NAME.settings" >> $UWSGI_FILE
echo "home = $PWD" >> $UWSGI_FILE
echo "master = true" >> $UWSGI_FILE
echo "processes = 1" >> $UWSGI_FILE
echo "socket = /tmp/$PROJECT_NAME.sock" >> $UWSGI_FILE
echo "chmod-socket = 666" >> $UWSGI_FILE
echo "daemonize = $PWD/logs/uwsgi.log" >> $UWSGI_FILE
echo "touch-reload = $PWD/reload_uwsgi" >> $UWSGI_FILE
echo "harakiri = 60" >> $UWSGI_FILE
echo "vacuum = True" >> $UWSGI_FILE
echo "buffer-size=32768" >> $UWSGI_FILE

# Create .gitignore
echo "/bin/*" >> .gitignore
echo "/include/*" >> .gitignore
echo "/lib/*" >> .gitignore
echo "/local/*" >> .gitignore

# Create nginx
echo "upstream $PROJECT_NAME {" >> $NGINX_FILE
echo "        server unix:///tmp/$PROJECT_NAME.sock;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "" >> $NGINX_FILE
echo "server {" >> $NGINX_FILE
echo "        listen 80;" >> $NGINX_FILE
echo "        server_name $PROJECT_NAME.ru www.$PROJECT_NAME.ru;" >> $NGINX_FILE
echo "        charset utf-8;" >> $NGINX_FILE
echo "        client_max_body_size 1m;" >> $NGINX_FILE
echo "        error_log $PWD/logs/nginx_error.log warn;" >> $NGINX_FILE
echo "        # access_log $PWD/logs/nginx.log main;" >> $NGINX_FILE
echo "" >> $NGINX_FILE
echo "        location / {" >> $NGINX_FILE
echo "                uwsgi_pass $PROJECT_NAME;" >> $NGINX_FILE
echo "                include uwsgi_params;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        location /media {" >> $NGINX_FILE
echo "                alias $PWD/www/media;" >> $NGINX_FILE
echo "                # expires 30d;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        location /static {" >> $NGINX_FILE
echo "                gzip_static on;" >> $NGINX_FILE
echo "                alias $PWD/www/static;" >> $NGINX_FILE
echo "                # expires 30d;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        location /robots.txt {" >> $NGINX_FILE
echo "                alias $PWD/www/robots.txt;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        location /favicon.ico {" >> $NGINX_FILE
echo "                alias $PWD/www/favicon.ico;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        error_page 404 /404.html;" >> $NGINX_FILE
echo "        location = /404.html {" >> $NGINX_FILE
echo "            root $PWD/www/templates/error;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "        error_page 500 502 503 504 /50x.html;" >> $NGINX_FILE
echo "        location = /50x.html {" >> $NGINX_FILE
echo "            root $PWD/www/templates/error;" >> $NGINX_FILE
echo "        }" >> $NGINX_FILE
echo "}" >> $NGINX_FILE

# Create file with settings server
echo "#!/bin/bash" >> $SETTINGS_SERVER
echo "# Add Nginx settings" >> $SETTINGS_SERVER
echo "sudo ln -s $PWD/nginx /etc/nginx/sites-enabled/$PROJECT_NAME" >> $SETTINGS_SERVER
echo "# Add Celery settings" >> $SETTINGS_SERVER
echo "sudo ln -s $PWD/supervisor.conf /etc/supervisor/conf.d/$PROJECT_NAME.conf" >> $DEPLOY_FILE
echo "# Add Uwsgi settings" >> $SETTINGS_SERVER
echo "sudo mkdir /etc/uwsgi" >> $SETTINGS_SERVER
echo "sudo mkdir /etc/uwsgi/vassals" >> $SETTINGS_SERVER
echo "sudo ln -s $PWD/uwsgi.ini /etc/uwsgi/vassals/$PROJECT_NAME.ini" >> $SETTINGS_SERVER
echo "sudo uwsgi --emperor /etc/uwsgi/vassals --uid www-data --gid www-data" >> $SETTINGS_SERVER
echo "# Reload Nginx" >> $SETTINGS_SERVER
echo "sudo service nginx reload" >> $SETTINGS_SERVER

# create supervisor
echo "[program:checkin-celery]" >> SUPERVISOR
echo "command=$PWD/bin/celery --app=$PROJECT_NAME worker -l ERROR" >> SUPERVISOR
echo "directory=$PWD/www" >> SUPERVISOR
echo "user=$USER_NAME" >> SUPERVISOR
echo "numprocs=1" >> SUPERVISOR
echo "stdout_logfile=$PWD/logs/celery.log" >> SUPERVISOR
echo "stderr_logfile=$PWD/celery_error.log" >> SUPERVISOR
echo "autostart=true" >> SUPERVISOR
echo "autorestart=true" >> SUPERVISOR
echo "startsecs=10" >> SUPERVISOR
echo "stopwaitsecs = 600" >> SUPERVISOR
echo "killasgroup=true" >> SUPERVISOR
echo "priority=998" >> SUPERVISOR

touch $PWD/logs/celery.log
touch $PWD/celery_error.log

# Git commit
git add .
git commit -m 'init'

echo 'All is good'